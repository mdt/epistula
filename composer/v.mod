Module {
	name: 'epistula-composer',
	description: 'The mail composer of epistula written in V.',
	version: '0.0.0',
	repo_url: 'https://codeberg.org/mdt/epistula/src/branch/master/composer',
	dependencies: []
}
