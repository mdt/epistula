module emdete.de/go/epistula-browser

go 1.21

require (
	github.com/arran4/golang-ical v0.0.0-20220115055431-e3ae8290e7b8
	github.com/gdamore/tcell/v2 v2.5.3
	github.com/mattn/go-runewidth v0.0.14
	github.com/proglottis/gpgme v0.1.1
	github.com/robfig/config v0.0.0-20141207224736-0f78529c8c7e
	github.com/sendgrid/go-gmime v0.0.0-20211124164648-4c44cbd981d8
	github.com/zenhack/go.notmuch v0.0.0-20211022191430-4d57e8ad2a8b
)

require (
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/sys v0.2.0 // indirect
	golang.org/x/term v0.2.0 // indirect
	golang.org/x/text v0.4.0 // indirect
)
